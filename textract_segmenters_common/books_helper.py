import json
import sys

from celery.exceptions import Ignore
from requests import HTTPError
from vedavaapi.client import VedavaapiSession


def get_page_ids(vc: VedavaapiSession, book_id=None, page_ranges=None, page_ids=None, update_state=None):
    if page_ids is not None:
        return page_ids
    if book_id is None:
        return []

    requested_indices = None
    if page_ranges:
        requested_indices = set()
        for r in page_ranges:
            if isinstance(r, int):
                requested_indices.add(r)
            elif isinstance(r, list):
                requested_indices.update(range(r[0], r[1]))

    seq_anno_resp = vc.get(
        'objstore/v1/resources',
        parms={
            "selector_doc": json.dumps(
                {"jsonClass": "SequenceAnnotation", "target": book_id, "canonical": "default_canvas_sequence"}),
            "projection": json.dumps({"jsonClass": 1, "_id": 1, "body": 1})}
    )

    if page_ranges and seq_anno_resp.status_code == 200 and seq_anno_resp.json()['items']:
        seq_anno = seq_anno_resp.json()['items'][0]
        seq_anno_members = seq_anno.get('body', {}).get('members', [])
        sequenced_ids = [m['resource'] for m in seq_anno_members]
        return [
            sequenced_ids[index] for index in requested_indices
            if isinstance(index, int) and index < len(sequenced_ids)
        ]

    else:
        selector_doc = { "jsonClass": "ScannedPage", "source": book_id }
        projection = { "selector": 1, "index": 1, "_id": 1 }
        pages_resp = vc.get(
            'objstore/v1/resources',
            parms={"selector_doc": json.dumps(selector_doc), "projection": json.dumps(projection)})
        try:
            pages_resp.raise_for_status()
        except HTTPError as e:
            if update_state:
                update_state(state='FAILURE', meta={'status': 'error in retrieving pages', 'error': e.response.json()})
            raise Ignore()

        pages = pages_resp.json()['items']

        return [
            pages[index]['_id'] for index in requested_indices
            if isinstance(index, int) and index < len(pages)
        ] if page_ranges else [p['_id'] for p in pages]


def get_page_image_id(vc, page_id, update_state=None):
    update_state = update_state or (lambda *args, **kwargs: None)

    update_state(state='PROGRESS', meta={"status": "retrieving page description", "count": 1, "total": 5})

    page_url = 'objstore/v1/resources/{}'.format(page_id)
    parms = { "projection": json.dumps({"permissins": 0}) }
    response = vc.get(page_url, parms=parms)
    if response.status_code != 200:
        update_state(
            state='FAILURE',
            meta={
                "status": "error in getting page info.",
                "error": response.json(), "code": response.status_code
            }
        )
        raise Ignore()

    resource_json = response.json()
    if 'representations' not in resource_json or 'stillImage' not in resource_json['representations']:
        update_state(state='FAILURE', meta={"status": "page doesn\'t have image representation"})
        raise Ignore()

    resource_image_oold_id = resource_json['representations']['stillImage'][0]['data']
    if not resource_image_oold_id.startswith('_OOLD:'):
        update_state(state='FAILURE', meta={"status": "page doesn\'t have image representation"})
        raise Ignore()

    resource_image_oold_id = resource_image_oold_id[6:]

    return resource_image_oold_id


def get_oold_url(vc, oold_id):
    file_url = 'objstore/v1/files/{}'.format(oold_id)
    abs_file_url = vc.abs_url(file_url)
    return abs_file_url


def get_oold(vc, oold_id):
    print('get_oold called')
    file_url = 'objstore/v1/files/{}'.format(oold_id)
    print('getting image file {}'.format(file_url), file=sys.stderr)
    file_response = vc.get(file_url)
    print("got image file ", len(file_response.content), file=sys.stderr)
    file_content = file_response.content

    return file_content


def delete_previous_regions(vc, page_id, update_state=None):
    from celery.exceptions import Ignore

    delete_request_data = {
        "filter_doc": json.dumps({"jsonClass": {"$in": ["Resource", "ImageRegion"]}, "state": "system_inferred"})
    }
    children_delete_response = vc.delete(
        'objstore/v1/resources/{}/specific_resources'.format(page_id), data=delete_request_data)

    update_state = update_state or (lambda *args, **kwargs: None)

    if children_delete_response.status_code != 200:
        update_state(state='FAILURE', meta={
            "exc_type": "ApiError",
            "exc_message": ["error in deleting existing image regions"],
        })
        raise Ignore()

    else:
        response_json = children_delete_response.json()
        if response_json.get('delete_status') and False in response_json.get('delete_status').values():
            update_state(state='FAILURE', meta={
                "exc_type": "PolicyError",
                "exc_message": ['cannot delete all existing regions'],
            })
            raise Ignore()
