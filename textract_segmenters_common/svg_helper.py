import uuid


class DCommand(object):

    def __init__(self, command, arg1, arg2, prev_dcommand=None):
        self.command = command
        self.is_relative = str(command).islower()

        self.prev_dcommand = prev_dcommand  # type: DCommand
        self.effective_command = None
        prev_x = prev_dcommand.x if prev_dcommand else 0
        prev_y = prev_dcommand.y if prev_dcommand else 0

        if self.is_relative:
            self.dx = arg1
            self.dy = arg2
            self.x = prev_x + self.dx
            self.y = prev_y + self.dy
        else:
            self.x = arg1
            self.y = arg2
            self.dx = self.x - prev_x
            self.dy = self.y - prev_y

    def d_str(self, alt_forms=True):
        c = ''
        if self.command in ('M', 'm'):
            self.effective_command = self.command
            if self.prev_dcommand and (self.command == (self.prev_dcommand.effective_command or self.prev_dcommand.command)):
                c += ''
            else:
                c += self.command
            c += '{},{} '.format(
                self.dx if self.is_relative else self.x, self.dy if self.is_relative else self.y)
            return c

        elif self.command in ('l', 'L'):
            if alt_forms:
                for arg in ('x', 'y'):
                    if self.prev_dcommand and (getattr(self, arg) == getattr(self.prev_dcommand, arg)):
                        alt_form = 'h' if arg == 'y' else 'v'
                        self.effective_command = alt_form
                        if self.prev_dcommand and (self.prev_dcommand.effective_command or self.command) == alt_form:
                            c += ''
                        else:
                            c += alt_form
                        c += '{} '.format(self.dx if alt_form == 'h' else self.dy)
                        return c

            self.effective_command = self.command
            if self.prev_dcommand and (
                    self.command == (self.prev_dcommand.effective_command or self.prev_dcommand.command)):
                c += ''
            else:
                c += self.command
            c += '{},{} '.format(
                self.dx if self.is_relative else self.x, self.dy if self.is_relative else self.y)
            return c

        return ''


def get_dcommands(points):
    d_commands = []

    prev_command = None
    for p in points:

        if not len(d_commands):
            d_command = DCommand('M', p[0], p[1])
            d_commands.append(d_command)
            prev_command = d_command
            continue

        dx = p[0] - prev_command.x
        dy = p[1] - prev_command.y
        d_command = DCommand('l', dx, dy, prev_dcommand=prev_command)
        d_commands.append(d_command)
        prev_command = d_command
        continue

    return d_commands


def svg_for_polygon(vertices):

    d_commands = get_dcommands(vertices)
    d_str = ''

    for d_command in d_commands:
        c = d_command.d_str()
        d_str+= c

    svg = '''<svg xmlns='http://www.w3.org/2000/svg'> <path xmlns="http://www.w3.org/2000/svg" d="{d}z" data-paper-data="{pd}" fill-opacity="0.00001" fill="#00bfff" fill-rule="nonzero" stroke="#00bfff" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal" id="{rpid}"/> </svg>'''.format(
        d=d_str,
        pd='{&quot;strokeWidth&quot;:1,&quot;editable&quot;:true,&quot;deleteIcon&quot;:null,&quot;annotation&quot;:null}',
        rpid='rough_path_{}'.format(uuid.uuid4().hex)
    )

    return svg


# TODO svg to vertices fn
