import os

from celery import Celery

def make_celery(flask_app, celery_app_path, include_modules):
    broker_url = os.environ.get('CELERY_BROKER_URL', 'redis://')
    backend_url = os.environ.get('CELERY_RESULT_BACKEND', 'redis://')

    celery_app = Celery(
        celery_app_path,
        broker=broker_url,
        backend=backend_url,
        include=include_modules[:]
    )
    celery_app.conf.update(
        CELERY_TASK_RESULT_EXPIRES=86400,
        CELERY_ACCEPT_CONTENT=['pickle'],
        CELERY_TASK_SERIALIZER='pickle',
        CELERY_RESULT_SERIALIZER='pickle'
    )

    class ContextTask(celery_app.Task):
        def __call__(self, *args, **kwargs):
            with flask_app.app_context():
                return self.run(*args, **kwargs)

    # noinspection PyPropertyAccess
    celery_app.Task = ContextTask
    return celery_app


def get_task_status(celery_app, task_id):
    result = celery_app.AsyncResult(task_id)
    if not result:
        return None

    #  print({"task_info": result.info})
    if result.state == 'PENDING':
        # job did not start yet
        response = {
            'state': result.state,
            'status': 'Pending...',
        }

    elif result.state != 'FAILURE':
        response = {
            'state': result.state
        }
    else:
        # something went wrong in the background job
        response = {
            'state': result.state,
            'status': str(result.info),  # this is the exception raised
        }

    if isinstance(result.info, dict):
        response.update(result.info)

    return response


def terminate_task(celery_app, task_id):
    return celery_app.control.revoke(task_id, terminate=True, signal='SIGUSR1')
